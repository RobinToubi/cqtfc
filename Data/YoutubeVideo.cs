// Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
using System.Text.Json.Serialization;

public class ContentDetails
{
    [JsonConstructor]
    public ContentDetails(string videoId, DateTime videoPublishedAt
    )
    {
        this.VideoId = videoId;
        this.VideoPublishedAt = videoPublishedAt;
    }

    [JsonPropertyName("videoId")]
    public string VideoId { get; }

    [JsonPropertyName("videoPublishedAt")]
    public DateTime VideoPublishedAt { get; }
}

#region Thumnails

public abstract class ThumbnailType
{
    [JsonPropertyName("url")]
    public string? Url { get; set; }

    [JsonPropertyName("width")]
    public int Width { get; set; }

    [JsonPropertyName("height")]
    public int Height { get; set; }
}

public class Standard : ThumbnailType
{
    [JsonConstructor]
    public Standard(string url, int width, int height
    )
    {
        this.Url = url;
        this.Width = width;
        this.Height = height;
    }
}

public class Default : ThumbnailType
{
    [JsonConstructor]
    public Default(string url, int width, int height
    )
    {
        this.Url = url;
        this.Width = width;
        this.Height = height;
    }
}

public class High : ThumbnailType
{
    [JsonConstructor]
    public High(string url, int width, int height
    )
    {
        this.Url = url;
        this.Width = width;
        this.Height = height;
    }
}

public class Maxres : ThumbnailType
{
    [JsonConstructor]
    public Maxres(string url, int width, int height
    )
    {
        this.Url = url;
        this.Width = width;
        this.Height = height;
    }
}

public class Medium : ThumbnailType
{
    [JsonConstructor]
    public Medium(string url, int width, int height)
    {
        this.Url = url;
        this.Width = width;
        this.Height = height;
    }
}
#endregion


public class Video
{
    [JsonConstructor]
    public Video(string kind, string etag, string id, Snippet snippet, ContentDetails contentDetails
    )
    {
        this.Kind = kind;
        this.Etag = etag;
        this.Id = id;
        this.Snippet = snippet;
        this.ContentDetails = contentDetails;
    }

    [JsonPropertyName("kind")]
    public string Kind { get; }

    [JsonPropertyName("etag")]
    public string Etag { get; }

    [JsonPropertyName("id")]
    public string Id { get; }

    [JsonPropertyName("snippet")]
    public Snippet Snippet { get; }

    [JsonPropertyName("contentDetails")]
    public ContentDetails ContentDetails { get; }
}



public class PageInfo
{
    [JsonConstructor]
    public PageInfo(int totalResults, int resultsPerPage
    )
    {
        this.TotalResults = totalResults;
        this.ResultsPerPage = resultsPerPage;
    }

    [JsonPropertyName("totalResults")]
    public int TotalResults { get; }

    [JsonPropertyName("resultsPerPage")]
    public int ResultsPerPage { get; }
}

public class ResourceId
{
    [JsonConstructor]
    public ResourceId(string kind, string videoId
    )
    {
        this.Kind = kind;
        this.VideoId = videoId;
    }

    [JsonPropertyName("kind")]
    public string Kind { get; }

    [JsonPropertyName("videoId")]
    public string VideoId { get; }
}

public class Root
{
    [JsonConstructor]
    public Root(string kind, string etag, string nextPageToken,
    List<Video> items, PageInfo pageInfo)
    {
        this.Kind = kind;
        this.Etag = etag;
        this.NextPageToken = nextPageToken;
        this.Videos = items;
        this.PageInfo = pageInfo;
    }

    [JsonPropertyName("kind")]
    public string Kind { get; }

    [JsonPropertyName("etag")]
    public string Etag { get; }

    [JsonPropertyName("nextPageToken")]
    public string NextPageToken { get; }

    [JsonPropertyName("items")]
    public IReadOnlyList<Video> Videos { get; }

    [JsonPropertyName("pageInfo")]
    public PageInfo PageInfo { get; }
}

public class Snippet
{
    [JsonConstructor]
    public Snippet(DateTime publishedAt, string channelId, string title, string description, Thumbnails thumbnails, string channelTitle, string playlistId, int position, ResourceId resourceId, string videoOwnerChannelTitle, string videoOwnerChannelId
    )
    {
        this.PublishedAt = publishedAt;
        this.ChannelId = channelId;
        this.Title = title;
        this.Description = description;
        this.Thumbnails = thumbnails;
        this.ChannelTitle = channelTitle;
        this.PlaylistId = playlistId;
        this.Position = position;
        this.ResourceId = resourceId;
        this.VideoOwnerChannelTitle = videoOwnerChannelTitle;
        this.VideoOwnerChannelId = videoOwnerChannelId;
    }

    [JsonPropertyName("publishedAt")]
    public DateTime PublishedAt { get; }

    [JsonPropertyName("channelId")]
    public string ChannelId { get; }

    [JsonPropertyName("title")]
    public string Title { get; }

    [JsonPropertyName("description")]
    public string Description { get; }

    [JsonPropertyName("thumbnails")]
    public Thumbnails Thumbnails { get; }

    [JsonPropertyName("channelTitle")]
    public string ChannelTitle { get; }

    [JsonPropertyName("playlistId")]
    public string PlaylistId { get; }

    [JsonPropertyName("position")]
    public int Position { get; }

    [JsonPropertyName("resourceId")]
    public ResourceId ResourceId { get; }

    [JsonPropertyName("videoOwnerChannelTitle")]
    public string VideoOwnerChannelTitle { get; }

    [JsonPropertyName("videoOwnerChannelId")]
    public string VideoOwnerChannelId { get; }
}

public class Thumbnails
{
    [JsonConstructor]
    public Thumbnails(Default @default, Medium medium, High high, Standard standard, Maxres maxres
    )
    {
        this.Default = @default;
        this.Medium = medium;
        this.High = high;
        this.Standard = standard;
        this.Maxres = maxres;
    }

    [JsonPropertyName("default")]
    public Default Default { get; }

    [JsonPropertyName("medium")]
    public Medium Medium { get; }

    [JsonPropertyName("high")]
    public High High { get; }

    [JsonPropertyName("standard")]
    public Standard Standard { get; }

    [JsonPropertyName("maxres")]
    public Maxres Maxres { get; }
}

