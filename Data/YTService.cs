using Google.Apis.YouTube.v3;
using Newtonsoft.Json;

namespace cqfc.Data;

public class YTService
{ 
    public YouTubeService youtubeService {get;set;}
    public Video[]? Videos { get; set; }

    public YTService() {
        this.youtubeService = new(new Google.Apis.Services.BaseClientService.Initializer());
    }
    public Task<Video[]?> GetPlaylistVideos()
    {
        string playlistText = System.IO.File.ReadAllText("wwwroot/playlist-items.json");
        Videos = JsonConvert.DeserializeObject<Video[]>(playlistText);
        return Task.FromResult(Videos);
    }
}